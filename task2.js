let name = '';
let age = '';

do {
  name = prompt('What is your name?');
} while (!name || !isNaN(name)); // поки ім'я не введено або не є числом, запитувати ім'я

do {
  age = prompt('What is your age?');
} while (!age || isNaN(age)); // поки вік не введено або є не числом, запитувати вік

if (age < 18) {
  alert('You are not allowed to visit this website.');
} else if (age >= 18 && age <= 22) {
  const confirmResult = confirm('Are you sure you want to continue?');
  if (confirmResult) {
    alert(`Welcome, ${name}!`);
  } else {
    alert('You are not allowed to visit this website.');
  }
} else {
  alert(`Welcome, ${name}!`);
}
